
# Repetições

## for 

### i)   qual o produto de maior valor?
### ii)  qual a soma total dos produtos do carrinho?
### iii) qual o valor médio do produto?
### iv)  qual o produto de menor valor?

## problema 1):
### "varrer"-> olhar cada elemento por vez (LOOP)
###  ter um valor inicial de referência-> 0
###  comparar cada valor e definir se é o maior ou não
###   armazenar o maior valor e ir para próximo item da lista
## foreach 

print("Produtos:")
maior_valor = 0
produto_de_maior_valor = ""

# para cada item no carrinho : faça <- 
for produto in carrinho:
    if maior_valor < produto[1]:
        maior_valor = produto[1]
        produto_de_maior_valor = produto[0]

print(f"Item de maior valor: {produto_de_maior_valor}")

maior_valor = 0
produto_de_maior_valor = ""

for indice in range(len(carrinho)):
        if carrinho[indice][1] > maior_valor:
            maior_valor = carrinho[indice][1]
            produto_de_maior_valor = carrinho[indice][0]

print(f"Item de maior valor: {produto_de_maior_valor}")

print("Fim da lista de compras")
print(f"Maior valor encontrado na lista: {maior_valor}")

# [ item1, item2, item3 ]
# 1: produto = item1 0 < 5.50? Sim, vou assumir que 5.50 é o maior valor
# 2: produto = item2 5.50 < 6.50? Sim, ....         6.50
#....
# 4: produto = item4 

## while


exit()
# variável de controle do loop
contador = 0

while contador < len(carrinho):
    print(carrinho[contador])
    print(contador)
    contador += 2 # lembrar de atualizar

# 1: contador: 0
#       0 < 4 ? SIM:
#           print(carrinho[0]) SKU1928...
#            contador = contador + 1 (0 = 0 + 1)
## 2: contador : 1
#       1 < 4 ? SIM:
#           print(carrinho[1])
#            contador += 1 (1 = 1 + 1)
#   3: contador: 2
#      2 < 4 ....
#
#   ...
#   4: contador: 4
#      4 < 4? Não!




## Problema 2: Qual a soma total do carrinho de compras? R: 33.4

### valor inicial -> 0 utilizando uma variável
### 1 Varredura item por item
###   1.1  a cada rodada pegar o valor do item
###   1.2           acumular o valor do item
### 2 apresentar o valor total

carrinho =[
        #0       1
  ('SKU#1928', 10.90), 
  ('SKU#1772', 5.50),
  ('SKU#2012', 9.50),
  ('SKU#1203', 7.50),
]

soma = 0

for produto in carrinho:
    soma += produto[1]

## Problema 3: Qual o valor médio dos produtos no carrinho atual
media = soma/len(carrinho)

print("O valor total do carrinho de compras : {}".format(soma))
print(f"O valor médio do produto é: {media}")



### iv)  qual o produto de menor valor?

menor_valor = carrinho[0][1]

for item in carrinho:
    if menor_valor > item[1]:
        menor_valor = item[1]

print("O menor valor encontrador foi de :", menor_valor)

## Perguntas:


#!/usr/bin/env python3

matriz = [
  [1,2,3],
  [4,5,6],
  [7,8,9]
]

#valor_diagonal = ?      # 1,5,7  l == c 
#valor_seg_diagonal = ?  # 3,5,7  

# elemento 3 -> (0,2) 0 + 2 = 2
# elemento 5 -> (1,1) 1 + 1 = 2
# elemento 7 -> (2,0) 2 + 0 = 2


# variável para armazenar a soma da diagonal principal
# variável para armazenar a soma da diagonal secundária
# percorrer cada linha
# para cada linha, percorrer cada coluna
# checar se as coordenadas de linha e coluna são iguais
  ## caso positivo: acumular o valor do elemento para diag. principal
# checar se as coordenadas de linha e coluna somam 2
  ## caso positivo: acumular o valor do elemento para a diag. secundária
# apresentar os resultados


l = 3
c = 3

dp = 0
ds = 0

             # (0,1,2)
for linha in range(l):
    for coluna in range(c): #(0,1,2)
        if linha == coluna:
            dp += matriz[linha][coluna]
        if linha + coluna == 2:
            ds += matriz[linha][coluna]

print(dp,ds)

# Iteração 1: linha =  0
  # segundo for :
  ## iteração 1
#              linha = 0 
#              coluna= 0
#              # 1
#     iteracao 2
#              linha = 0
#              coluna = 1
#              # 2
#
#     iteracao 3:
#              linha = 0
#              coluna = 2
#              # 3
#
## Iteração2: linha = 1
#   # segundo for:
#  ## iteração 1
#              linha = 1 
#              coluna= 0
#              # 4
#     iteracao 2
#              linha = 1
#              coluna = 1
#              # 5
#
#     iteracao 3:
#              linha = 1
#              coluna = 2
#              # 6
## Iteração3: linha = 2
#   # segundo for:
#  ## iteração 1
#              linha = 2 
#              coluna= 0
#              # 4
#     iteracao 2
#              linha = 2
#              coluna = 1
#              # 5
#
#     iteracao 3:
#              linha = 2
#              coluna = 2
#              # 6



### Solução do Bruno:

matriz = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

coluna = len(matriz[0])
linha = len(matriz)

valor_diagonal = []
valor_seg_diagonal = []

aux = 1

for x in range(coluna):
    for y in range(linha):
        if (x == y):
            valor_diagonal.append(matriz[x][y])
            valor_seg_diagonal.append(matriz[x][-aux])
            aux += 1

print(sum(valor_diagonal))
print(sum(valor_seg_diagonal)) 



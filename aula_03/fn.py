# PEP484

# função customizada

CONSTANTES = "letra maiuscula"

# Objetivo -> unidade mínima de modularização 
## -> organização ; definir responsabilidades
### do one thing and do it well;

##### -> trecho código generalizável / reutilizar


## Spreads 

dic = {'n1': 10, 'n2': 20}
tupla = (0,1,2,3,4,5)

                             # key=value, key2 = value,....)
def soma_a_partir_de_keywords(**kwargs): # keyword arguments
    print(kwargs)
    print(kwargs.keys()) 
                             # value1, value2,value3, .....
def soma_com_multiplos_argumentos(*args): # multiple arguments
    print(args)
    print(type(args)) # tupla
    print(args[0])
    soma = 0
    for i in args: # operações com as tuplas
        soma += i
    return soma
                                # Spreading para dicionários
print(soma_a_partir_de_keywords(**dic))
                                # Spreading para tuplas
print(soma_com_multiplos_argumentos(*tupla)
                                    # valor padrão -> depois dos valores obrigatórios
def soma_de_dois_numeros(n2 : int = 2 , n1: int = 2) -> int: # parametros
    print(n1)
    print(n2)
    #n1 += 10
    return n1 + n2
    # mais de uma instrução


num1 = 13
num2 = 1234


print(soma_de_dois_numeros(num1,num2)) # 1247 -> 1257
print(num1) # 13 por conta de ser imutável

def soma_a_partir_de_uma_lista(lista_de_numeros: list) -> int:
    soma = 0
    lista_de_numeros[0] = lista_de_numeros[0] + 10
    for num in lista_de_numeros:
        soma += num
    return soma

lista = [1,2,3,4,5,6]

print(soma_a_partir_de_uma_lista(lista))
print("qual o valor do primeiro elemento da lista?", lista[0]) #11 (10 + 1)


## Hint -> "Dica" => Indicação dos tipos
def qualquer(string: str) -> None:
    print("Valor informado", string)


# tipos imutáveis -> passagem de valores por cópia -> o valor será alterado
# tipos mutáveis  -> passagem por referência  -> o objeto em si é passível de alteração

# 13 -> n1
# 1234 -> n2
# 13 + 1234 
# retornar o resultado da soma



      # argumentos
print("mensagem") # call operator 

# Função anônima

variavel_qualquer = lambda param1, param2:  param1 + param2# instrução do que a função realiza

variavel_qualquer(1,2)

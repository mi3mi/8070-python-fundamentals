
### Criar uma lista de ceps e montar um arquivo no format csv 
#### Coletar dados da API do VIACEP
#### Lista de 10 ceps
#### realizar a requisição via requests
#### conventer a saída json para um formato adequado ao csv
#### salvar o arquivo localmente

## definir os parametros de busca
## realizar as requisições de todos os ceps
## preparar o arquivo de saida
### cabeçalho
### formato de registro
### armazenamento em dico


import requests


api_url = 'http://viacep.com.br/ws/{}/json/'

# 1. definir a lista de cep
ceps = [
    '80530230', 
    '22241200',
    '05508220',
    '06310390',
    '04101300',
    '01310200',
    '05005001',
    '04094005',
    '06223040',
    '06213070',
    '99999999'
]

# Formatação
cabec = 'cep,logradouro,complemento,bairro,localidade,uf,ibge,gia,ddd,siafi\n'
dados = []

dados.append(cabec)


# Coleta dos dados
for cep in ceps:
    #print(f"http://viacep.com.br/ws/{cep}/json")
    resposta = requests.get(api_url.format(cep))
    payload = resposta.json()
    # if resposta.status_code == 200:
    if 'erro' not in payload.keys():
        registro = f"{payload.get('cep')}," \
                   f"{payload.get('logradouro')}," \
                   f"{payload.get('complemento')}," \
                   f"{payload.get('bairro')}," \
                   f"{payload.get('localidade')}," \
                   f"{payload.get('uf')}," \
                   f"{payload.get('ibge')}," \
                   f"{payload.get('gia')}," \
                   f"{payload.get('ddd')}," \
                   f"{payload.get('siafi')}\n" 
        dados.append(registro)


with open("ceps.csv", "x") as fp:
    fp.writelines(dados)

#ex requisição
#resposta = requests.get(api_url.format('04101300'))


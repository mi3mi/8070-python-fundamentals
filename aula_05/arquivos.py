

# 2 maneiras principais

## open():

# abertura                # path do arquivo
buffer = open('arquivo.txt', 'rt')
                            # opções

# leitura
#conteudo = buffer.read()
conteudo = buffer.readlines()
# fechamento
buffer.close()

# adicionar uma linha nova no conteúdo
nome = "Giovanni" # inputs...
idade = "33"
cidade = "Vila dos Remédios"
estado = "SP"

nova_linha = "{};{};{};{}\n".format(nome,idade,cidade,estado)
print(nova_linha)

conteudo.append(nova_linha)

# abre novamente um stream...
arquivo = open('novo_arquivo.txt', 'x')

#arquivo.write(string) 
arquivo.writelines(conteudo)
                    #lista
# fecha o arquivo
arquivo.close()




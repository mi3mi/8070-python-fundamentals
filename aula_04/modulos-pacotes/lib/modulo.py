#!/usr/bin/env python3 

# Para o Python , um módulo é qualquer arquivo que contenha instruções 
# de python.

def soma(x,y):
    return x + y

def subtracao(x,y):
    return x - y

# (....)

if __name__ == "__main__":
    print("isso é um módulo")





from os import getenv

import paramiko
import dotenv

# carregar as informações de variável de ambiente do arquivo .env local
dotenv.load_dotenv()

# dados sensitivos

HOST = getenv('HOST_IP')
USERNAME = getenv('SSH_USER')
PASSWORD = getenv('USER_PASSWD')
PORT = getenv('PORT')


# inicializar client do paramiko
client = paramiko.SSHClient()

# adicionar a maquina ao arquivo known hosts
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# criar a conexão
client.connect(HOST, port=PORT, username=USERNAME, password=PASSWORD)

# executar um comando no host 
response = client.exec_command('cat /etc/os-release')

stdin, stdout, stderr = response

# inspecionar a saída do comando:
output = stdout.readlines()




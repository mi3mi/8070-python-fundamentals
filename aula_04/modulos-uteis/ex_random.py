
import random
import time

from random import randint as ri


# gerar números inteiros aleatórios 

n1 = random.randint(0,199)
print(n1)

# embaralhar listas

lista = [ x for x in range(1000)  ]

random.shuffle(lista)

print(lista)

# selecionar amostras 

amostra = [ lista[random.randint(x,len(lista)-1)] for x in range(0,10)  ]

print(amostra)

# dados mocks

names = ['Julio', 'André', 'Felipe', 'Fernando', 'Maria', 'Cecília']

users = [
    {
        'id': f'{ri(0,9)}{ri(0,9)}{ri(0,9)}',
        'name': names[ri(0,len(names)-1)],

    } for x in range(0,100)
]



# Jogos 

cadeiras = len(names) - 1

print("Jogo começou!")

while cadeiras >= 1:
    #jogo....
    print("Música esta rolando.... ♪♪♪")
    time.sleep(1)
    print('.....♪♪♪.....')
    time.sleep(2)
    print('.....♪♪♪.....')
    time.sleep(1)

    print("Música PAROU!")
    time.sleep(1)

    print("Os participantes se movimentam")

    time.sleep(1)

    # algum jogador tem que ser retirado....
    random.shuffle(names)

    time.sleep(1)
    
    print(f"O jogador {names.pop()} saiu do jogo!")

    cadeiras -=1


print(f"O jogador {names[0]} Venceu!")










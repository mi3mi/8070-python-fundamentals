## Desafio

Coletar os dados de 6 meses do historical data da B3:

- https://www.b3.com.br/en_us/market-data-and-indices/data-services/market-data/historical-data/equities/historical-quote-data/


- Observar o layout do arquivo no seguinte link: https://www.b3.com.br/data/files/65/50/AD/26/29C8B51095EE46B5790D8AA8/HistoricalQuotations_B3.pdf

Os dados deverão ser armazenados no mongo atlas: https://atlas.mongodb.com

Cada documento do banco terão os seguintes campos:

- Data do pregão (DATA EXCHANGE)
- Código do papel (CODNEG)
- Nome reduzido (NOMERES)
- Preço de Abertura (PREABE)
- Preço Máximo (PREMAX)
- Preço Mínimo (PREMIN)
- Preço Médio (PREMED)
- Último preço (PREULT)

Desejáveis:

- Testes 
- Modularizado -> OOP/FN
- Boas práticas -> PEP8/Dados Sensitivos


----

criar uma api que consome o banco

-> consultar ações por mes
-> consultar ações por codneg
-> consultar por períodos
-> incluir ação
-> deletar ação .....
-> ranking no período das ações que mais valorizaram...


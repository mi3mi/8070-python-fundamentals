#!/usr/bin/python3

#
#
# Que receba dois números e apresente a sua soma.
#
#

## Pensar Computacionalmente 

## problema -> problema de computação -> linguagem (python)

## Algoritmo

## 1: receber o primeiro número     <> input() resultado da captura de dados -> string  
## 2: armazenar o número informado  <variável>  = input()
n1 = input("Informe o Primeiro Número: ")

## 3: receber o segundo número
## 4: armazenar o número informado
n2 = input('Informe o Segundo Número: ')

## 5: efetuar a soma e armazenar o seu resultado em um formato numérico
soma = float(n1) + float(n2)

## 6: apresenta a soma para o usuário final
print("O resultado da soma {} + {} = {:.2f}".format(n1,n2,soma))

# F'String v3.6+
print(f"O resultado da soma {n1} + {n2} = {soma:.2f}  ") # <-

print("O resultado da soma {pri} + {sec} = {sum}".format(pri=n1, sec=n2, sum=soma))





### definir uma lista de cep - pelo menos 10 <adicionei no repositório> 
### consultar na api do viacep
### persistir no mongodb local -> storage cep / db cep / cep
### estrutura de dicionário:
###
#      { cep, logradouro, bairro, uf, ddd, complemento  }
#
#      não precisa pegar siagi, ibge, gia
#
#      adicionar timestamp da coleta

import datetime
import requests

from time import sleep
from MontyDB import MontyDB

FILENAME = 'lista_ceps.txt'
API_URL = 'http://viacep.com.br/ws/{}/json/'
keys = ['cep', 'logradouro', 'complemento', 'bairro' , 'localidade','uf' ,'ddd']

def get_data(filename: str) -> "TextIOWrapper" :
    return open(filename)

hasNext = True
data = []

fp = get_data(FILENAME)

while hasNext:
    try:
        cep = next(fp).strip()
        response  = requests.get(API_URL.format(cep))
        response.raise_for_status()

    except StopIteration:
        hasNext = False
        fp.close()

    except requests.exceptions.HTTPError as e:
        print(e)

    else:
        response_data = response.json()
        target = { key : response_data.get(key) for key in keys    }
        target['timestamp'] = datetime.datetime.utcnow()
        data.append(target)
        sleep(1)
        print(target)
    


try:
    with MontyDB('cep', 'cep', 'ceps') as mdb:
        mdb.collection.insert_many(data)

except Exception as e:
    print(e)



        






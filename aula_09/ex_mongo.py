
import pymongo

from os import getenv

from dotenv import load_dotenv

load_dotenv()


client = pymongo.MongoClient(getenv('MONGO_URI'))

db = client.get_database('video')

collection = client.get_collection('movieDetails')


import unittest

from ex_exceptions import divisao

class TesteDivisao(unittest.TestCase):

    def setUp(self):
        self.x = 20
        self.y = 10

    def makeAssertions(self):
        self.assertEqual('Não dividirás por zero',divisao(self.x,0))

    def test_ex_exceptions(self):
        """ não realiza divisões por zero"""
        self.makeAssertions()

    def test_exemplo(self):
        """ realiza divisão com números """
        self.assertEqual(2, divisao(self.x,self.y))


#!/usr/bin/env python3



def divisao(num1:int, num2:int) -> float :
    try: 
        return num1/num2
    except ZeroDivisionError:
        return "Não dividirás por zero"

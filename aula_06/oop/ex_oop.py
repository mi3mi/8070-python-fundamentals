

#####
## Criar uma classe que representa uma conta bancária
## definam os atributos
## definam os comportamentos

###
###[*] consultar saldo
###[*] sacar dinheiro
###[*] deposito em dinheiro
###[*] transferencia entre contas
###

class ContaBancaria:
    def __init__(self):
        self._saldo = 0
        self._limite = 0

    @property # getter
    def saldo(self):
        return self._saldo

    @saldo.setter
    def saldo(self, valor):
        if isinstance(valor, float):
            self._saldo = valor
        else:
            print('não foi') 
    def verificarSaldo(self):
        return self._saldo

    def sacar(self, valor: float) -> None or str :
        if (self._limite + self._saldo) >= valor:
            self._saldo -= valor
        else:
            return 'Limite Indisponível para Saque'

    def depositar(self, valor: float):
        self._saldo += valor

    def transferir(self, valor: float,
            conta_destino: "ContaBancaria"
    ):
        self._saldo -= valor
        conta_destino.depositar(valor)

def qualquerFuncao(conta: ContaBancaria):
    pass


#### Exemplo de uma conta transferir dinheiro para outra.

## Exercício iniciado pelo erik
#class ContaBancaria:
#    def __init__ (self):
#        self.conta = 0
#        self.agencia = 0
#        self.titular = ''
#        self.tipo = ''
#        self._saldo = 0
#        self._limiteEspecial = 0
#
#    def consultarSaldo(self, saldo):
#        print(f'Seu saldo é {saldo}')
#    def sacar(self, valorSaque: int = 100):
#        #saqueDesejado = float(input('Quanto deseja sacar?'))
#        self._saldo -= valorSaque
#    def depositar(self, valorDeposito: int = 100):
#        self._saldo += valorDeposito
#    def transferir(self, ContaBancaria): 

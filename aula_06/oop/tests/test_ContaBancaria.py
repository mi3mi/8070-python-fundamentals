
from unittest import TestCase
from ex_oop import ContaBancaria


class TesteContaBancaria(TestCase):
    
    def setUp(self):
        self.conta = ContaBancaria() # objetos mutáveis
        self.conta_destino = ContaBancaria()

    def testConsultarSaldo(self):
        """ Deve apresentar o saldo em conta  """
        self.assertEqual(0, self.conta.verificarSaldo())

    def testSacarDinheiro(self):
        """ Possibilita a retirada de valores da conta """
        self.conta.depositar(200)
        saque = 100
        valor_inicial = self.conta.verificarSaldo() # 200
        self.conta.sacar(saque)
        self.assertEqual(valor_inicial - saque, self.conta.verificarSaldo())

    def testChecaLimiteAntesDeSacar(self):
        valor_inicial = self.conta.verificarSaldo()
        valor = 100000 
        self.assertEqual('Limite Indisponível para Saque',self.conta.sacar(valor))
        self.assertEqual(valor_inicial,self.conta.verificarSaldo())

    def testRealizarDeposito(self):
        """ Possibilita adicionar valor em conta """
        deposito = 100
        self.conta.depositar(deposito)
        self.assertEqual(100, self.conta.verificarSaldo())

    def testRealizarTransferencia(self):
        """ Possibilita a realização de transferência de valores entre contas  """
        valor_inicial = 0
        transferencia = 100
        # conta origem
        self.conta.transferir(transferencia, self.conta_destino)
        self.assertEqual(valor_inicial - transferencia, self.conta.verificarSaldo())
        # conta destino
        self.assertEqual(valor_inicial + transferencia, 
                self.conta_destino.verificarSaldo())



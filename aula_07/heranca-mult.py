
class Pai:
    def __init__(self):
        self.idade = 0
        self.nacionalidade = 'brasileira'

    def apresentacao(self):
        return 'Olá como vai você!'

    def outroMetodo(self):
        return f"Minha idade é {self.idade}"


class Mae:
    def __init__(self):
        self.idade = 0,
        self.nacionalidade = 'inglesa'

    def apresentacao(self):
        return 'Hi, How are you?'

    def ondeMora(self):
        return 'In São Paulo'


class Filha(Mae,Pai):
    def apresentacao(self):
        return ' Olá, my name is ...'


class N1(Filha):
    def apresentacao(self):
        return ' E Aí mano!'


class N2(Filha):
    def apresentacao(self):
        return ' Bah tchê!'

class B1(N1,N2):
    pass




